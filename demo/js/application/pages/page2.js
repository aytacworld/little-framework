(function () {
  'use strict';

  var page = new lf.Page(2, '/detail/:id', './js/application/pages/page2.html', ['bookService', 'cartService'], {
    _setup: function(self) {
      self.renderTemplate();
      self.book = new lf.O(self.html, 'book');
      self.amount = new lf.O(self.html, 'quantity');

      self.addToCartBtn = self.html.find('.add-to-cart');
      self.quantityField = self.html.find('.quantity');

      self.addToCartBtn.on('click', self.onAddToCartBtnClicked);
    },

    _start: function(self) {
      self.amount(1);
      var book = self.bookService.getBook(self.parameters.id);
      self.book(book);
    },

    onAddToCartBtnClicked: function() {
      var self = page;
      var quantity = self.amount();
      if (quantity) {
        self.cartService.add(quantity, self.book());
      }
    }
  });

  app.addPage(page);
})();
