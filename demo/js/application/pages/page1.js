(function () {
  'use strict';

  var page = new lf.Page(1, '/books', './js/application/pages/page1.html', ['bookService'], {
    _start: function(self) {
      self.bookService.getAll()
        .then(function(books) {return self.renderTemplate(books);});
    }
  });

  app.addPage(page, true);
})();
