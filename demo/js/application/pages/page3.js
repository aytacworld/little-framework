(function () {
  'use strict';

  var page = new lf.Page(3, '/cart', './js/application/pages/page3.html', ['bookService', 'cartService'], {
    _start: function(self) {
      var books = self.cartService.getOrderedBooks();
      self.renderTemplate(books);
    }
  });

  app.addPage(page);
})();
