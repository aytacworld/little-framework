(function () {
  'use strict';

  var cartService = function() {

    var self = {};

    var cart = [];

    self.add = function(quantity, book) {
      cart.push({book: book, quantity: quantity, total: quantity * book.price});
    };

    self.getOrderedBooks = function() {
      return cart;
    };

    return self;

  };

  app.addService(new lf.Service('cartService', cartService()));
})();
