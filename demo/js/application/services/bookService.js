(function () {
  'use strict';

  var bookService = function() {

    var self = {};

    var books = [];

    self.getAll = function() {
      var def = lf.Q.deferred();
      if (books.length > 0) {
        def.resolve(books);
      } else {
        lf._.ajax('./Server/books.json', {
          method: 'GET',
          onSuccess: function(res) {
            books = res.data;
            def.resolve(books);
          },
          onError: function(err) {
            def.reject(err);
          }
        });
      }
      return def.promise();
    };

    self.getBook = function(id) {
      for (var i = 0, max = books.length; i < max; i++) {
        if (books[i].id === id) {
          return books[i];
        }
      }
      return null;
    };

    return self;

  };

  app.addService(new lf.Service('bookService', bookService()));
})();
