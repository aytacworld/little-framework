// Application region
lf.Application = function(settings, rootScope) {
  'use strict';

  settings = settings || {};
  this.currentIndex = 0;
  this.currentPage = null;
  this.autoLoadFirstPage = settings.autoLoadFirstPage || false;
  this.pages = [];
  this.services = [];
  this.router = new lf.Router();
  this.rootScope = rootScope;
};

lf.Application.prototype.init = function() {
  'use strict';

  var self = this;
  this.router.onRouteChanged(function(e) {
    self.open(self.currentPage, e.page, e.parameters);
  });
};

lf.Application.prototype.addPage = function(p, mainPage) {
  'use strict';

  if (p instanceof lf.Page) {
    var page = this.injectServices(p);
    page.rootScope = this.rootScope;
    page.next = this.createNextFunction(page); // wizard method
    page.previous = this.createPreviousFunction(page); // wizard method
    this.pages.push(page);
    this.router.addRoute(page.route, page, mainPage);
    if (this.autoLoadFirstPage) {
      this.router.autoload();
    }
  }
  return this;
};

lf.Application.prototype.addService = function(s) {
  'use strict';

  if (s instanceof lf.Service) {
    if (s !== null) {
      this.services.push(s);
    }
  }
  return this;
};

lf.Application.prototype.open = function(previous, next, parameters) {
  'use strict';

  if (previous) {
    previous.html.hide();
  }
  next.parameters = parameters;
  next.html.show();
  next.resume();
  this.currentPage = next;
};

lf.Application.prototype.getPage = function(id) {
  'use strict';

  for (var i = 0, max = this.pages.length; i < max; i++) {
    if (this.pages[i].id === id) {
      return this.pages[i];
    }
  }
  return null;
};

lf.Application.prototype.getService = function(name) {
  'use strict';

  for (var i = 0, max = this.services.length; i < max; i++) {
    if (this.services[i].name === name) {
      return this.services[i];
    }
  }
  return null;
};

lf.Application.prototype.injectServices = function(p) {
  'use strict';

  for (var i = 0, max = p.services.length; i < max; i++) {
    var serv = this.getService(p.services[i]);
    if (serv !== null) {
      p[p.services[i]] = serv;
    }
  }
  return p;
};

// wizard section (refactor in future) region
lf.Application.prototype.next = function() {
  'use strict';

  this.open(this.pages[this.currentIndex], this.pages[this.currentIndex + 1]);
  this.currentIndex = this.currentIndex + 1;
};

lf.Application.prototype.previous = function() {
  'use strict';

  this.open(this.pages[this.currentIndex], this.pages[this.currentIndex - 1]);
  this.currentIndex = this.currentIndex - 1;
};

lf.Application.prototype.createNextFunction = function(p) {
  'use strict';

  var self = this;
  return function() {
    if (p._next) {
      p._next();
    }
    self.next();
  };
};

lf.Application.prototype.createPreviousFunction = function(p) {
  'use strict';

  var self = this;
  return function() {
    if (p._previous) {
      p._previous();
    }
    self.previous();
  };
};
// wizard section (refactor in future) endregion

// Application endregion
