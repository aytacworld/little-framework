// Application region
lf.Router = function() {
  'use strict';

  this.pages = [];
  this.currentUrl = '';
  this.fireOnRouteChanged = null;
  this.init();
  this.autoloaded = false;
};

lf.Router.prototype.init = function() {
  'use strict';

  var self = this;

  lf._.addEvent(window, 'hashchange', function(e) {
    self.routePage(lf._.getUrlHash(e.newURL));
  });
};

lf.Router.prototype.addRoute = function(route, page, mainPage) {
  'use strict';

  this.pages.push({route: route, page: page, main: mainPage});
};

lf.Router.prototype.getUrlParameters = function(url, pattern) {
  'use strict';

  var allUrlSplit = (url || '').split('?')[0];
  var allPatternSplit = pattern.split('?')[0];
  var hashUrlSplit = allUrlSplit.split('/');
  var hashPatternSplit = allPatternSplit.split('/');

  var valid = true;
  var params = {};

  if (hashUrlSplit.length === hashPatternSplit.length) {
    for (var i = 0, max = hashPatternSplit.length; valid && i < max; i++) {
      var item = hashPatternSplit[i];
      if (item[0] === ':') {
        params[item.substring(1)] = hashUrlSplit[i];
      } else if (item !== hashUrlSplit[i]) {
        valid = false;
      }
    }

    if (valid) {
      return params;
    }
  }

  return null;
};

lf.Router.prototype.getQueryParameters = function(url) {
  'use strict';

  var queryRaw = (url || '').split('?')[1];
  if (queryRaw === undefined) {
    return {};
  }
  var querySplit = queryRaw.split('&');
  var paramList = {};
  for (var i = 0, max = querySplit.length; i < max; i++) {
    var query = querySplit[i].split('=');
    paramList[query[0]] = query[1];
  }

  return paramList;
};

lf.Router.prototype.onRouteChanged = function(cb) {
  'use strict';

  this.fireOnRouteChanged = cb;
};

lf.Router.prototype.autoload = function() {
  'use strict';

  var self = this;
  if (!self.autoloaded) {
    var newUrl = lf._.getUrlHash(window.location.hash);
    self.routePage(newUrl, true, newUrl === undefined);
    // route to default page if routing fails, but after the first load, skip the start if already loaded
    // var loadSuccess = self.routePage(newUrl, true, newUrl === undefined);
    // if (!loadSuccess) {
    //   self.routePage(newUrl, true, true);
    // }
  }
};

lf.Router.prototype.routePage = function(newUrl, autoload, main) {
  'use strict';

  var self = this;
  var toMainPage = autoload === true && main === true || newUrl === undefined;
  for (var i = 0, max = self.pages.length; i < max; i++) {
    var p = self.pages[i];
    var params = toMainPage ? {} : self.getUrlParameters(newUrl, p.route);
    if (((toMainPage) || (params !== null && self.currentUrl !== newUrl)) && self.fireOnRouteChanged !== null) {
      var qParams = self.getQueryParameters(newUrl);
      lf._.extend(params, qParams);
      self.currentUrl = toMainPage ? p.route : newUrl;
      self.fireOnRouteChanged({page: p.page, parameters: params});
      if (autoload) {self.autoloaded = true;}
      return true;
    }
  }
  return false;
};

// Application endregion
