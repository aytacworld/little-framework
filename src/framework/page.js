// Page region
lf.Page = function(id, route, template, services, extension) {
  'use strict';

  this.id = id;
  this.init = false;
  this.html = lf.$('.page.page-' + id);
  this.route = route;
  this.parameters = null;
  this.services = services;
  this.template = template;
  this.rootScope = {};

  lf._.extend(this, extension);
};

lf.Page.prototype.setup = function() {
  'use strict';

  var def = lf.Q.deferred();
  var self = this;

  if (self.template !== null && self.template !== '') {
    self.getTemplate()
      .then(function() {
        if (self._setup) {
          self._setup(self);
        }
        def.resolve();
      });
  } else {
    if (self._setup) {
      self._setup(self);
    }
    def.resolve();
  }
  return def.promise();
};

lf.Page.prototype.resume = function() {
  'use strict';

  var self = this;
  if (!self.init) {
    self.init = true;
    self.setup()
      .then(function() {self.start();});
  } else {
    self.start();
  }
};

lf.Page.prototype.start = function() {
  'use strict';

  if (this._start) {
    this._start(this);
  }
};

lf.Page.prototype.getTemplate = function() {
  'use strict';

  var def = lf.Q.deferred();
  var self = this;
  lf._.ajax(self.template, {
    method: 'GET',
    onSuccess: function(html) {
      self.template = html;
      def.resolve();
    },
    onError: function(err) {
      def.reject(err);
    }
  });
  return def.promise();
};

lf.Page.prototype.renderTemplate = function(data) {
  'use strict';

  var renderedHtml = lf.T.render(this.template, data);
  this.html.html(renderedHtml);
};

// Page endregion
