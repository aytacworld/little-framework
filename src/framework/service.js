// Service region
lf.Service = function(name, extension) {
  'use strict';

  this.name = name;

  lf._.extend(this, extension);
};
// Service endregion
