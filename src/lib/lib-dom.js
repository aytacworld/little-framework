lf.internals.Wrapper = function(elements) {
  'use strict';

  var self = this;
  self.elements = null;
  if (elements.length === 1) {
    self.elements = elements[0];
  } else {
    self.elements = elements;
  }
  self.events = {};
};

lf.internals.Wrapper.prototype.html = function(innerHTML) {
  'use strict';

  var self = this;

  if (innerHTML) {
    // SET
    if (!self.elements.length || self.elements.length === 1) {
      self.elements.innerHTML = innerHTML;
    } else {
      for (var i = 0, max = self.elements.length; i < max; i++) {
        self.elements[i].innerHTML = innerHTML;
      }
    }
  } else {
    //GET
    if (!self.elements.length || self.elements.length === 1) {
      return self.elements.innerHTML;
    }
    var ret = [];
    for (var i2 = 0, max2 = self.elements.length; i2 < max2; i2++) {
      ret[i2] = self.elements[i2].innerHTML;
    }
    return ret.join('');
  }
};

lf.internals.Wrapper.prototype.val = function(val) {
  'use strict';

  var self = this;

  if (val !== undefined) {
    // SET
    if (!self.elements.length || self.elements.length === 1) {
      if (self.elements.tagName === 'INPUT' || self.elements.tagName === 'TEXTAREA') {
        self.elements.value = val;
      } else {
        self.elements.innerHTML = val;
      }
    } else {
      for (var i = 0, max = self.elements.length; i < max; i++) {
        if (self.elements[i].tagName === 'INPUT' || self.elements[i].tagName === 'TEXTAREA') {
          self.elements[i].value = val;
        } else {
          self.elements[i].innerHTML = val;
        }
      }
    }
  } else {
    //GET
    if (!self.elements.length || self.elements.length === 1) {
      if (self.elements.tagName === 'INPUT' || self.elements.tagName === 'TEXTAREA') {
        return self.elements.value;
      }
      return self.elements.innerHTML;
    }
    var ret = [];
    for (var i2 = 0, max2 = self.elements.length; i2 < max2; i2++) {
      if (self.elements[i2].tagName === 'INPUT' || self.elements[i2].tagName === 'TEXTAREA') {
        ret[i2] = self.elements[i2].value;
      } else {
        ret[i2] = self.elements[i2].innerHTML;
      }
    }
    return ret.join('');
  }
};

lf.internals.Wrapper.prototype.on = function(eventName, handlerFunction) {
  'use strict';

  var self = this;

  self.events[eventName] = self.events[eventName] || [];
  self.events[eventName].push(handlerFunction);

  if (!self.elements.length || self.elements.length === 1) {
    lf._.addEvent(self.elements, eventName, handlerFunction);
  } else {
    for (var i = 0, max = self.elements.length; i < max; i++) {
      lf._.addEvent(self.elements[i], eventName, handlerFunction);
    }
  }
};

lf.internals.Wrapper.prototype.off = function(eventName) {
  'use strict';

  var self = this;

  if (self.events[eventName]) {
    for (var i = self.events[eventName].length - 1; i >= 0; i--) {
      var fn = self.events[eventName][i];
      if (!self.elements.length || self.elements.length === 1) {
        lf._.removeEvent(self.elements, eventName, fn);
      } else {
        for (var i2 = 0, max = self.elements.length; i2 < max; i2++) {
          lf._.removeEvent(self.elements[i2], eventName, fn);
        }
      }
    }

    delete self.events[eventName];
  }
};

lf.internals.Wrapper.prototype.show = function(condition) {
  'use strict';

  this._toggleVisible(condition, true);
};

lf.internals.Wrapper.prototype.hide = function(condition) {
  'use strict';

  this._toggleVisible(condition, false);
};

lf.internals.Wrapper.prototype._toggleVisible = function(condition, show) {
  'use strict';

  var self = this;
  var option = show === true ? 'block' : 'none';

  if (condition && condition === true || condition === undefined) {
    if (!self.elements.length || self.elements.length === 1) {
      self.elements.style.display = option;
    } else {
      for (var i = 0, max = self.elements.length; i < max; i++) {
        self.elements[i].style.display = option;
      }
    }
  }
};

lf.internals.Wrapper.prototype.find = function(selector) {
  'use strict';

  var self = this;

  return lf.$(selector, self.elements);
};

lf.$ = function(elementName, root) {
  'use strict';

  if (elementName !== undefined && elementName !== null && elementName !== '') {
    root = root || document;
    var elements = root.querySelectorAll(elementName);

    var returnElement = elements !== null && elements.length > 0 ? new lf.internals.Wrapper(elements) : null;
    return returnElement;
  }
  return null;
};
