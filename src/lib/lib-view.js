lf.internals.TemplateEngine = function() {
  'use strict';

};

// http://krasimirtsonev.com/blog/article/Javascript-template-engine-in-just-20-line

lf.internals.TemplateEngine.prototype.render = function (html, options) {
  'use strict';

  var re = /<%([^%]+)?%>/g;
  // var reExp = /(^( )?(if|for|else|switch|case|break|var|{|}))(.*)?/g; // regexp with keywords
  var reExp = /(^( )?(=))(.*)?/g;
  var code = 'var r=[];var model = this.model;\n';
  var cursor = 0;
  var match = re.exec(html);
  var add = function(line, js) {
    if (js) {
      (code += line.match(reExp) ? 'r.push(' + line.substr(1) + ');\n' : line + '\n');
    } else {
      (code += line != '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
    }
    return add;
  };
  while (match) {
    add(html.slice(cursor, match.index))(match[1] || match[2], true);
    cursor = match.index + match[0].length;
    match = re.exec(html);
  }
  add(html.substr(cursor, html.length - cursor));
  code += 'return r.join("");';
  return new Function(code.replace(/[\r\t\n]/g, '')).apply({model: options});
};

lf.T = new lf.internals.TemplateEngine();
