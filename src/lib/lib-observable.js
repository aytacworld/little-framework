lf.internals.Observable = function(initVal, onUpdate) {
  'use strict';

  var self = this;
  self.value = initVal;
  self.fireUpdate = onUpdate;
  return function(val, skipUpdate) {
    if (val === undefined) {
      return self.value;
    }
    var oldVal = self.value;
    self.value = val;
    if ((!skipUpdate) && self.fireUpdate) {
      self.fireUpdate(oldVal, val);
    }
  };
};

lf.O = function(root, modelName) {
  'use strict';

  var ret;

  function setDomElements() {
    var elements = root.find('[data-model="' + modelName + '"]');

    if (elements !== null) {
      elements.on('input', function(e) {
        var val = e.target.value;

        e.preventDefault();

        ret(val, true);
      });
    }
  }

  function updateModelElement(nVal, prop) {
    var element = root.find('[data-model="' + prop + '"]');

    if (element !== null) {
      element.val(nVal);
    }
  }

  function updateModel (newValue) {
    if (typeof newValue === 'object') {
      for (var prop in newValue) {
        updateModelElement(newValue[prop], modelName + '.' + prop);
      }
    } else {
      updateModelElement(newValue, modelName);
    }
  }

  function updateVisibilityElement (nVal, prop) {
    var show = root.find('[data-show="' + prop + '"]');
    var hide = root.find('[data-hide="' + prop + '"]');

    if (show !== null) {
      if (nVal) {
        show.show();
      } else {
        show.hide();
      }
    }
    if (hide !== null) {
      if (nVal) {
        hide.hide();
      } else {
        hide.show();
      }
    }
  }

  function updateVisibility (newValue) {
    if (typeof newValue === 'object') {
      for (var prop in newValue) {
        updateVisibilityElement(newValue, modelName + '.' + prop);
      }
    }
    updateVisibilityElement(newValue, modelName);
  }

  ret = new lf.internals.Observable(null, function(o, n) {
    updateVisibility(n);
    updateModel(n);
  });

  setDomElements();

  return ret;
};
