lf.Q = function(onSuccess, onFail) {
  'use strict';

  this.level = 1;
  this.onSuccess = onSuccess;
  this.onFail = onFail;
  this.onCatch = function(err) { throw new Error(err); };
  this.next = null;
  this.delayTimestamp = 0;
};

lf.Q.prototype.then = function(onSuccess, onFail) {
  'use strict';

  this.next = new lf.Q(onSuccess, onFail);
  this.next.level = this.level + 1;
  return this.next;
};

lf.Q.prototype.delay = function(ts) {
  'use strict';

  this.next = new lf.Q();
  this.next.level = this.level + 1;
  this.next.delayTimestamp = ts;
  return this.next;
};

// TODO refactor
lf.Q.prototype.fireSuccessCallback = function(val) {
  'use strict';

  var self = this;
  if (this.next === null && this.level > 1) {
    return;
  } else if (this.next === null && this.level === 1) {
    try {
      setTimeout(function() {
        var res = self.next.onSuccess(val);
        self.next.fireSuccessCallback(res);
      }, 200);
    } catch (err) {
      this.onCatch(err);
    }
  } else {
    try {
      if (this.next.delayTimestamp > 0) {
        setTimeout(function() {
          self.next.fireSuccessCallback(val);
        }, this.next.delayTimestamp);
      } else {
        var res = this.next.onSuccess(val);
        this.next.fireSuccessCallback(res);
      }
    } catch (err) {
      this.onCatch(err);
    }
  }
};

// TODO refactor
lf.Q.prototype.fireErrorCallback = function(err) {
  'use strict';

  if (this.next === null) {
    return;
  }
  try {
    var res = this.next.onFail(err);
    this.next.fireErrorCallback(res);
  } catch (err2) {
    this.onCatch(err2);
  }
};

lf.Q.deferred = function() {
  'use strict';

  return new lf.internals.Deferred();
};

lf.internals.Deferred = function() {
  'use strict';

  this.prom = new lf.Q();
};

lf.internals.Deferred.prototype.promise = function() {
  'use strict';

  return this.prom;
};

lf.internals.Deferred.prototype.resolve = function(val) {
  'use strict';

  this.prom.fireSuccessCallback(val);
};

lf.internals.Deferred.prototype.reject = function(val) {
  'use strict';

  this.prom.fireFailCallback(val);
};
