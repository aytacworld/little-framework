lf.internals.Util = function() {
  'use strict';

};

lf.internals.Util.prototype.contains = function(obj, searchText) {
  'use strict';

  return obj.indexOf(searchText) > -1;
};

lf.internals.Util.prototype.ajax = function(url, settings) {
  'use strict';

  if (url === undefined || url === null) {
    throw 'url is undefined';
  }
  var self = this;
  settings = settings || {};
  var opt = {
    url: url,
    method: settings.method || 'GET',
    data: settings.data,
    successCallback: settings.onSuccess,
    errorCallback: settings.onError,
    contentType: settings.contentType || 'text/plain'
  };

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState === 4) {
      if (xhttp.status >= 200 && xhttp.status <= 299) {
        if (opt.successCallback) {
          if (self.isJson(xhttp.responseText)) {
            opt.successCallback(JSON.parse(xhttp.responseText));
          } else {
            opt.successCallback(xhttp.responseText);
          }
        }
      } else if (opt.errorCallback) {
        var ret = {
          status: xhttp.status,
          text: xhttp.statusText,
          message: xhttp.responseText,
          url: xhttp.responseURL
        };
        opt.errorCallback(ret);
      } else {
        throw 'ajax error: ' + xhttp.responseURL + ' [' + xhttp.statusText + '(' + xhttp.status + ')]: ' + xhttp.responseText;
      }
    }
  };

  xhttp.open(opt.method, opt.url, true);
  xhttp.setRequestHeader('Content-type', opt.contentType);

  if (opt.method === 'POST' || opt.method === 'PUT') {
    xhttp.send(JSON.stringify(opt.data));
  } else {
    xhttp.send();
  }
};

lf.internals.Util.prototype.isJson = function(str) {
  'use strict';

  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};

lf.internals.Util.prototype.extend = function() {
  'use strict';

  var dest = arguments[0];

  for (var i = 1, max = arguments.length; i <= max; i++) {
    var src = arguments[i];

    for (var srcProp in src) {
      dest[srcProp] = src[srcProp];
    }
  }

  return dest;
};

lf.internals.Util.prototype.getUrlHash = function(url) {
  'use strict';

  return url.split('#')[1];
};

lf.internals.Util.prototype.addEvent = function(element, event, cb) {
  'use strict';

  if (element.addEventListener) {                // For all major browsers, except IE 8 and earlier
    element.addEventListener(event, cb);
  } else if (element.attachEvent) {              // For IE 8 and earlier versions
    element.attachEvent('on' + event, cb);
  }
};

lf.internals.Util.prototype.removeEvent = function(element, event, cb) {
  'use strict';

  if (element.removeEventListener) {         // For all major browsers, except IE 8 and earlier
    element.removeEventListener(event, cb);
  } else if (element.detachEvent) {          // For IE 8 and earlier versions
    element.detachEvent('on' + event, cb);
  }
};

lf._ = new lf.internals.Util();
