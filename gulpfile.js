'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});
var runSequence = require('run-sequence');
var args = require('yargs').argv;

var config = {
  srcPackages: './package.json',
  bumpOptions: {type: 'patch'},
  bumpDemoOptions: {type: 'pre'},
  connectOptions: {
    root: './demo',
    livereload: true
  },
  srcJsCore: './src/lib/*.js',
  srcJsFramework: './src/framework/*.js',
  srcJsApp: ['./demo/js/application/app.js', './demo/js/application/services/*.js', './demo/js/application/pages/*.js'],
  dstJsCore: './demo/scripts/lf-core.js',
  dstJsFramework: './demo/scripts/lf-framework.js',
  dstJsApp: './demo/scripts/app.js',

  srcCssStyle: ['./src/style/style.scss', './src/style/*.scss'],
  dstCssStyle: './demo/styles/style.css',
  srcCssTheme: ['./src/theme/theme.scss', './src/theme/*.scss'],
  dstCssTheme: './demo/styles/theme.css',

  srcDemo: [
    './demo/index.html',
    './demo/js/application/pages/*.html',
    './demo/css/overrides.css'
  ],
  dstDemoStyle: './demo/styles/',

  distSrc: [
    './demo/scripts/lf-*.js',
    './demo/scripts/lf-*.css',
    './demo/styles/*'
  ],
  distDest: './dist/'
};


// region Default

gulp.task('default', ['connect', 'watch']);

gulp.task('build', function(cb) {
  if (args.minify) {
    runSequence(
      'minify-core',
      'minify-framework',
      'minify-app',
      'minify-style',
      'minify-theme',
      function (err) {
        if (err) {
          throw err;
        }
        cb();
      });
  } else {
    runSequence(
      'concat-core',
      'concat-framework',
      'concat-app',
      'sass-style',
      'sass-theme',
      function (err) {
        if (err) {
          throw err;
        }
        cb();
      });
  }
});

gulp.task('watch', function() {
  gulp.watch(config.srcJsCore, ['bump-core']);
  gulp.watch(config.srcJsFramework, ['bump-framework']);
  gulp.watch(config.srcJsApp, ['bump-app']);
  gulp.watch(config.srcCssStyle, ['bump-style']);
  gulp.watch(config.srcCssTheme, ['bump-theme']);
  gulp.watch(config.srcDemo, ['bump-demo']);
});

gulp.task('connect', function() {
  $.connect.server(config.connectOptions);
});

gulp.task('bump', function(){

 /**
 * Bump the version
 * --type=pre will bump the prerelease version *.*.*-x
 * --type=patch or no flag will bump the patch version *.*.x
 * --type=minor will bump the minor version *.x.*
 * --type=major will bump the major version x.*.*
 * --version=1.2.3 will bump to a specific version and ignore other flags
 */

  var options = {};

  if (args.version) {
    options.version = args.version;
  } else if (args.major) {
    options.type = 'major';
  } else if (args.minor) {
    options.type = 'minor';
  } else if (args.patch) {
    options.type = 'patch';
  } else {
    options.type = 'pre';
  }
  return gulp.src(config.srcPackages)
    .pipe($.bump(options))
    .pipe(gulp.dest('./'));
});

gulp.task('release', ['minify-core', 'minify-framework', 'minify-app', 'minify-style', 'minify-theme'], function() {
  return gulp.src(config.distSrc)
    .pipe(gulp.dest(config.distDest));
});

// endregion Default

// region Linting

gulp.task('lint-core', function() {
  return gulp.src(config.srcJsCore)
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.eslint.failOnError());
});

gulp.task('lint-framework', function() {
  return gulp.src(config.srcJsFramework)
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.eslint.failOnError());
});

gulp.task('lint-app', function() {
  return gulp.src(config.srcJsApp)
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.eslint.failOnError());
});

// endregion Linting

// region Concat

gulp.task('concat-core', ['lint-core'], function() {
  return gulp.src(config.srcJsCore)
        .pipe($.concat(config.dstJsCore))
        .pipe(gulp.dest('./'));
});

gulp.task('concat-framework', ['lint-framework'], function() {
  return gulp.src(config.srcJsFramework)
        .pipe($.concat(config.dstJsFramework))
        .pipe(gulp.dest('./'));
});

gulp.task('concat-app', ['lint-app'], function() {
  return gulp.src(config.srcJsApp)
        .pipe($.concat(config.dstJsApp))
        .pipe(gulp.dest('./'));
});

// endregion Concat

// region Sass

gulp.task('sass-style', function() {
  return gulp
    .src(config.srcCssStyle)
    .pipe($.sass())
    // .pipe($.autoprefixer({browsers: ['last 2 version', '> 5%']}))
    .pipe($.concat(config.dstCssStyle))
    .pipe(gulp.dest('./'));
});

gulp.task('sass-theme', function() {
  return gulp
    .src(config.srcCssTheme)
    .pipe($.sass())
    // .pipe($.autoprefixer({browsers: ['last 2 version', '> 5%']}))
    .pipe($.concat(config.dstCssTheme))
    .pipe(gulp.dest('./'));
});

// endregion Sass

// region Minify

gulp.task('minify-core', ['lint-core'], function() {
  return gulp.src(config.srcJsCore)
        .pipe($.sourcemaps.init())
        .pipe($.concat(config.dstJsCore))
        .pipe(gulp.dest('./'))
        .pipe($.uglify())
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest('./'));
});

gulp.task('minify-framework', ['lint-framework'], function() {
  return gulp.src(config.srcJsFramework)
        .pipe($.sourcemaps.init())
        .pipe($.concat(config.dstJsFramework))
        .pipe(gulp.dest('./'))
        .pipe($.uglify())
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest('./'));
});

gulp.task('minify-app', ['lint-app'], function() {
  return gulp.src(config.srcJsApp)
        .pipe($.sourcemaps.init())
        .pipe($.concat(config.dstJsApp))
        .pipe(gulp.dest('./'))
        .pipe($.uglify())
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest('./'));
});

gulp.task('minify-style', ['sass-style'], function() {
  return gulp.src(config.dstCssStyle)
    .pipe($.csso())
    .pipe(gulp.dest(config.dstDemoStyle));
});

gulp.task('minify-theme', ['sass-theme'], function() {
  return gulp.src(config.dstCssTheme)
    .pipe($.csso())
    .pipe(gulp.dest(config.dstDemoStyle));
});

// endregion Minify

// region Bump

gulp.task('bump-core', ['concat-core'], function() {
  return gulp.src(config.srcPackages)
    .pipe($.bump(config.bumpOptions))
    .pipe(gulp.dest('./'))
    .pipe($.connect.reload());
});

gulp.task('bump-framework', ['concat-framework'], function() {
  return gulp.src(config.srcPackages)
    .pipe($.bump(config.bumpOptions))
    .pipe(gulp.dest('./'))
    .pipe($.connect.reload());
});

gulp.task('bump-app', ['concat-app'], function() {
  return gulp.src(config.srcPackages)
    .pipe($.bump(config.bumpDemoOptions))
    .pipe(gulp.dest('./'))
    .pipe($.connect.reload());
});

gulp.task('bump-style', ['sass-style'], function() {
  return gulp.src(config.srcPackages)
    .pipe($.bump(config.bumpOptions))
    .pipe(gulp.dest('./'))
    .pipe($.connect.reload());
});

gulp.task('bump-theme', ['sass-theme'], function() {
  return gulp.src(config.srcPackages)
    .pipe($.bump(config.bumpOptions))
    .pipe(gulp.dest('./'))
    .pipe($.connect.reload());
});

gulp.task('bump-demo', function() {
  return gulp.src(config.srcPackages)
    .pipe($.bump(config.bumpDemoOptions))
    .pipe(gulp.dest('./'))
    .pipe($.connect.reload());
});

// endregion Bump
